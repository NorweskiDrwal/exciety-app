import facebook from './facebook';
import firebase from './firebase';

export const configs = {
    facebook,
    firebase,
};
