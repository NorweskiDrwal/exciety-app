import {
    FIREBASE_API_KEY,
    FIREBASE_AUTH_DOMAIN,
    FIREBASE_DATABASE_URL,
    FIREBASE_PROJECT_ID,
    FIREBASE_STORAGE_BUCKET,
    FIREBASE_MESSAGING_SENDER_ID,
} from 'react-native-dotenv';

const firebase = Object.freeze({
    apiKey: FIREBASE_API_KEY as string || '',
    authDomain: FIREBASE_AUTH_DOMAIN as string || '',
    databaseURL: FIREBASE_DATABASE_URL as string || '',
    projectId: FIREBASE_PROJECT_ID as string || '',
    storageBucket: FIREBASE_STORAGE_BUCKET as string || '',
    messagingSenderId: FIREBASE_MESSAGING_SENDER_ID as string || '',
});

export default firebase;
