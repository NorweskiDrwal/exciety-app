export interface AppState {
    readonly isAuthenticated: boolean;
}
