import { combineReducers } from 'redux';
import thunk, { ThunkDispatch } from 'redux-thunk';
import { createStore, applyMiddleware, compose, DeepPartial, Action, Store } from 'redux';

import { StateType } from './types';

import app from './reducer';

const combinedReducers = () => combineReducers({
    app,
});

const rootReducer = combinedReducers();
const middlewares = [thunk];

export type RootState = StateType<typeof rootReducer>;

type StoreExtensions = { dispatch: ThunkDispatch<RootState, void, Action> };
type ExtendedStore = Store<RootState, Action> & StoreExtensions;

const reduxStore = (initialState?: DeepPartial<RootState>): ExtendedStore => {
    return createStore(
        rootReducer,
        initialState,
        compose(applyMiddleware(...middlewares)),
    );
};

export default reduxStore();
