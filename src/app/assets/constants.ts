export const Colors = Object.freeze({
    red: '#e24e42',
    blue: '#008f95',
    pink: '#eb6e80',
    info: '#4895ec',
    white: '#f5f5f5',
    error: '#ec5f59',
    google: '#ea4335',
    yellow: '#e9b000',
    purple: '#997792',
    success: '#67ac5b',
    midgrey: '#d8d8d8',
    warning: '#f6c244',
    darkgrey: '#5a6268',
    facebook: '#4a67ad',
});

export const Spacing = Object.freeze({
    S: '4px',
    M: '8px',
    ML: '12px',
    L: '16px',
    LXL: '20px',
    XL: '24px',
    XXL: '32px',
    XXXL: '50px',
});

export const Sizes = Object.freeze({
    header: '14px',
    paragraph: '12px',
});

export const Fonts = Object.freeze({
    main: 'Josefin Sans',
});
