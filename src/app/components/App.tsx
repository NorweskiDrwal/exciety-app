import * as React from 'react';
import { AppLoading, Asset, Font } from 'expo';
import { Provider as StoreProvider } from 'react-redux';
import { Platform, StatusBar, View } from 'react-native';

import store from 'src/app/store';
import { AccessContainer } from 'src/access/components';

interface Props {
    skipLoadingScreen: boolean;
}

interface State {
    isLoadingComplete: boolean;
}

class App extends React.Component<Props, State> {
    public state = {
        user: null,
        isLoadingComplete: false,
    };

    public render() {
        if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
            return (
                <AppLoading
                    startAsync={this.loadResourcesAsync}
                    onError={this.handleLoadingError}
                    onFinish={this.handleFinishLoading}
                />
            );
        }

        return (
            <StoreProvider store={store}>
                {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
                <AccessContainer/>
            </StoreProvider>
        );
    }

    private loadResourcesAsync = async () => {
        await Promise.all([
            Asset.loadAsync([]),
            Font.loadAsync({
                'josefin-sans': require('../assets/fonts/JosefinSans-Regular.ttf'),
                MaterialCommunityIcons: require('@expo/vector-icons/fonts/MaterialCommunityIcons.ttf'),
            }),
        ]);
    }

    private handleLoadingError = (error: any) => console.warn(error);

    private handleFinishLoading = () => this.setState({ isLoadingComplete: true });
}

export default App;
