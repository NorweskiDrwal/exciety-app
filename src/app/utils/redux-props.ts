import { Action } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { MapStateToProps } from 'react-redux';

import { RootState } from 'src/app/store';

export type Thunk = ThunkAction<Promise<any>, RootState, void, Action>;

type MapDispatch = Dictionary<Creator<Thunk> | Creator<Action>>;
type MapState = MapStateToProps<any, any, RootState>;

export type ReduxProps<S extends MapState, D extends MapDispatch = {}>
    = ReturnType<S> & D;
