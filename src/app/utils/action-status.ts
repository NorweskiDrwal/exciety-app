export enum ActionStatus {
    PENDING = 'PENDING',
    LOADING = 'LOADING',
    SUCCESS = 'SUCCESS',
    FAILURE = 'FAILURE',
}
