import { Reducer } from 'redux';

import { ActionStatus } from 'src/app/utils';

import { AccessState } from 'src/access/types';
import * as AccessActions from 'src/access/actions';

const initialState: AccessState = {
    user: null,
    authStatus: ActionStatus.PENDING,
};

const accessReducer: Reducer<AccessState> = (state = initialState, action): AccessState => {
    if (AccessActions.AUTH_USER_START.is(action)) {
        return { ...state, authStatus: ActionStatus.LOADING };
    }
    if (AccessActions.AUTH_USER_FAIL.is(action)) {
        return { ...state, authStatus: ActionStatus.FAILURE };
    }
    if (AccessActions.AUTH_USER_SUCCESS.is(action)) {
        return { ...state, authStatus: ActionStatus.SUCCESS, user: action.payload };
    }
    return state;
};

export default accessReducer;
