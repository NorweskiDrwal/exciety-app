import * as React from 'react';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import styled from 'styled-components/native';
import { View, Button, Image, Text } from 'react-native';

// import { RootState } from 'src/app/store';
import { Colors } from 'src/app/assets';
import { ReduxProps } from 'src/app/utils';
import { Facebook, Google, Login } from 'src/access/components';
import AuthService from 'src/services/authentication';

// const mapState = (state: RootState) => ({});
const mapState = () => ({});

const mapDispatch = {};

type Props = ReduxProps<typeof mapState, typeof mapDispatch>;
interface State {
    user: firebase.User | null;
}

class AccessContainer extends React.Component<Props, State> {
    public state: State = {
        user: null,
    };

    public componentDidMount() {
        AuthService.subscribeAuthChange(user => this.setState({ user }));
    }

    render() {
        const { user } = this.state;

        if (user) {
            const avatar = user.photoURL && (
                <Image style={{ width: 50, height: 50 }} source={{ uri: user.photoURL }} />
            );

            return (
                <Styled.Container>
                    <Text>You are logged in!</Text>
                    {avatar}
                    <Button onPress={AuthService.logout} title="Logout" />
                </Styled.Container>
            );
        }


        return (
            <Styled.Container>
                <Login/>
                <Facebook loginWithFacebook={AuthService.loginWithFacebook}/>
                <Google/>
            </Styled.Container>
        );
    }
}
const Styled = {
    Container: styled.View`
        flex: 1;
        display: flex;
        position: relative;
        align-items: flex-start;
        justify-content: flex-start;
        background-color: ${Colors.midgrey};
    `,
};

export default connect(mapState, mapDispatch)(AccessContainer);
