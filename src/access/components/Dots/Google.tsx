import * as React from 'react';
import styled from 'styled-components/native';
import { Animated, TouchableWithoutFeedback, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { Colors } from 'src/app/assets';

interface Props {}
interface State {
    spring: Animated.Value;
}

class Facebook extends React.Component<Props, State> {
    state = {
        spring: new Animated.Value(0.1),
    };

    handleOnMount = () => {
        Animated.spring(this.state.spring, {
            toValue: 1,
            friction: 3,
        }).start();
    }

    handlePressIn = () => {
        Animated.spring(this.state.spring, {
            toValue: .8,
            friction: 3,
            tension: 40,
        }).start();
    }

    handlePressOut = () => {
        Animated.spring(this.state.spring, {
            toValue: 1,
            friction: 3,
            tension: 40,
        }).start();
    }

    render() {
        const AnimatedDot = (by: Animated.Value) => {
            const spring = { transform: [{ scale: by }] };
            return (
                <StyledAnimatedView style={[spring]}>
                    <Styled.GoogleIcon name="google" size={60} color="white"/>
                </StyledAnimatedView>
            );
        };

        return (
            <TouchableWithoutFeedback
                onLayout={this.handleOnMount}
                onPressIn={this.handlePressIn}
                onPressOut={this.handlePressOut}
            >
                {AnimatedDot(this.state.spring)}
            </TouchableWithoutFeedback>
        );
    }
}

const Styled = {
    GoogleDot: styled.View`
        right: 50px;
        width: 70px;
        height: 70px;
        bottom: 50px;
        position: absolute;
        border-radius: 100px;
        background-color: ${Colors.google};
    `,
    GoogleIcon: styled(MaterialCommunityIcons)`
        left: 0;
        top: 3px;
        right: 0;
        bottom: 0;
        margin: auto;
    `,
};

const StyledAnimatedView = Animated.createAnimatedComponent(Styled.GoogleDot);

export default Facebook;
