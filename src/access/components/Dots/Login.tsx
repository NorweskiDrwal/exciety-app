import * as React from 'react';
import styled from 'styled-components/native';
import { Animated, TouchableWithoutFeedback, View, Text, Image } from 'react-native';

import { Colors } from 'src/app/assets';
// @ts-ignore
import Icon from 'src/app/assets/images/icon.png';

interface Props {}
interface State {
    spring: Animated.Value;
}

class Login extends React.Component<Props, State> {
    public state = {
        spring: new Animated.Value(0.1),
    };

    handleOnMount = () => {
        Animated.spring(this.state.spring, {
            toValue: 1,
            friction: 3,
        }).start();
    }

    handlePressIn = () => {
        Animated.spring(this.state.spring, {
            toValue: .8,
            friction: 3,
            tension: 40,
        }).start();
    }

    handlePressOut = () => {
        Animated.spring(this.state.spring, {
            toValue: 1,
            friction: 3,
            tension: 40,
        }).start();
    }

    render() {
        const AnimatedDot = (by: Animated.Value) => {
            const spring = { transform: [{ scale: by }] };
            return (
                <StyledAnimatedView style={[spring]}>
                    {/*<Styled.LoginText style={{ fontFamily: 'josefin-sans' }}>LOGIN</Styled.LoginText>*/}
                    <Styled.LoginImage source={Icon}/>
                </StyledAnimatedView>
            );
        };

        return (
            <TouchableWithoutFeedback
                onLayout={this.handleOnMount}
                onPressIn={this.handlePressIn}
                onPressOut={this.handlePressOut}
            >
                {AnimatedDot(this.state.spring)}
            </TouchableWithoutFeedback>
        );
    }
}

const Styled = {
    LoginDot: styled.View`
        left: 0;
        right: 0;
        top: 150px;
        width: 150px;
        height: 150px;
        margin: 0 auto;
        text-align: center;
        border-radius: 100px;
        background-color: ${Colors.red};
    `,
    LoginImage: styled.Image`
        top: 8px;
        right: 0;
        bottom: 0;
        left: 3px;
        width: 110px;
        margin: auto;
        height: 110px;
    `,
    LoginText: styled.Text`
        left: 0;
        top: 4px;
        right: 0;
        bottom: 0;
        color: #fff;
        margin: auto;
        font-size: 30px;
    `,
};

const StyledAnimatedView = Animated.createAnimatedComponent(Styled.LoginDot);

export default Login;
