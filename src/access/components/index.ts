import Login from './Dots/Login';
import Google from './Dots/Google';
import Facebook from './Dots/Facebook';
import AccessContainer from './Access.container';

export {
    Login,
    Google,
    Facebook,
    AccessContainer,
};
