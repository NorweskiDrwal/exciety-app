import * as firebase from 'firebase';
import { ActionDefinition } from 'src/app/types';

export const AUTH_USER_START = ActionDefinition.of('AUTH_USER_START');
export const AUTH_USER_FAIL = ActionDefinition.of('AUTH_USER_FAIL');
export const AUTH_USER_SUCCESS = ActionDefinition.of<firebase.User>('AUTH_USER_SUCCESS');
