import * as firebase from 'firebase';

export interface AccessState {
    readonly authStatus: string;
    readonly user: firebase.User | null;
}
