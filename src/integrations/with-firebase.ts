import * as firebase from 'firebase';

import { configs } from 'src/configs';

firebase.initializeApp(configs.firebase);

export const Firebase = firebase;
