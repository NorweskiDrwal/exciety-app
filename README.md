# exciety-app

####1. Frontend<br>
- React-Native/<a href="https://expo.io/">Expo</a><br>
    First you have to install Expo CLI to be able to run the project:<br>
    <pre><code>npm install -g expo-cli</code></pre>
    The project is using npm, so <code>npm install</code> is in order.<br>
    After that it's a simple case of running <code>expo start</code>
####2. Backend<br>
- Firebase<br>
The backend is based on Google's <a href="https://firebase.google.com/">Firebase</a>.<br>
All the necessary docs can be found on their page.
####3. Dependencies<br>
The list of major dependencies used:

- <code>redux</code>
- <code>redux-thunk</code>
- <code>firebase</code>
- <code>typescript</code>
- <code>tslint</code>
- <code>jest</code>
####4. Additional stuff<br>
For developer's convenience, here's a list of stuff configured for use:

- redux store
- absolute paths
- <code>react-native-dotenv</code> for storing configs in <code>.env</code> files
- scripts added to <code>package.json</code> for linting and tests

